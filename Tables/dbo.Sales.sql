CREATE TABLE [dbo].[Sales]
(
[SaleID] [int] NOT NULL IDENTITY(1, 1),
[EmpID] [int] NOT NULL,
[SaleDateTime] [datetime] NOT NULL,
[SaleAmount] [float] NULL
) ON [PRIMARY]
GO
