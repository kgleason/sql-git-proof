CREATE TABLE [dbo].[Employees]
(
[EmpID] [int] NOT NULL IDENTITY(1, 1),
[EmpName] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
[EmpStartDate] [datetime] NOT NULL,
[EmpPhone] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL
) ON [PRIMARY]
GO
