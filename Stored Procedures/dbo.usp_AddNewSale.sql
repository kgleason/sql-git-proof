
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE PROCEDURE [dbo].[usp_AddNewSale] @EmpID INT, @SaleAmount FLOAT, @TotalSales INT OUTPUT
AS
INSERT INTO Sales (EmpID, SaleDateTime, SaleAmount)
SELECT @EmpID, GETDATE(), @SaleAmount

SELECT @TotalSales=COUNT(1) FROM dbo.Sales WHERE EmpID = @EmpID
GO
