
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_NULLS ON
GO

CREATE VIEW [dbo].[SalesByUser] AS
SELECT s.SaleID,s.SaleDateTime, e.EmpName, e.EmpID,s.SaleAmount
FROM Sales s
JOIN Employees e
ON s.EmpID = e.EmpID

GO
